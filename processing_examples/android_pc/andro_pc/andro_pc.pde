// Example by Tom Igoe

import processing.net.*;

int port = 8081;       
Server myServer;        

void setup()
{
  size(1000, 1000);
  background(0);
  myServer = new Server(this, port);
}
int remoteX=0;
int remoteY=0;
void draw()
{
  // Get the next available client
  Client thisClient = myServer.available();
  // If the client is not null, and says something, display what it said
  if (thisClient !=null) {
    background(0);
  //  String whatClientSaid = thisClient.readString();
  String whatClientSaid = thisClient.readStringUntil('\n');
  whatClientSaid=trim(whatClientSaid);
    if (whatClientSaid != null) {
      println(thisClient.ip() + "received message:" + whatClientSaid);
      String[] list = split(whatClientSaid," ");
      println(list);
      remoteX=int(list[0]);
      remoteY=int(list[1]);
    } 
    
  } 
  
  ellipse(remoteX, remoteY, 55, 55);
}
