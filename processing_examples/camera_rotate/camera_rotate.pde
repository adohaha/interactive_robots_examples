/**
 * <p>Ketai Sensor Library for Android: http://KetaiProject.org</p>
 *
 * <p>Ketai Camera Features:
 * <ul>
 * <li>Interface for built-in camera</li>
 * <li></li>
 * </ul>
 * <p>Updated: 2012-03-10 Daniel Sauter/j.duran</p>
 */

import ketai.camera.*;
KetaiCamera cam;



/// copy copy

import ketai.ui.*;  //(1)
import android.view.MotionEvent;  //(2)

KetaiGesture gesture;  //(3)
float rectSize = 100;  //(4)
float rectAngle = 0.0;  
int x, y;
color c = color(255);  //(5)
color bg = color(78, 93, 75);  //(6)




////
void setup() {
  orientation(LANDSCAPE);
  cam = new KetaiCamera(this, 640, 480, 30);  //(1)
  imageMode(CENTER);  //(2)
  cam.start();
  
  //
  rectAngle=0.3;
  gesture = new KetaiGesture(this);  //(7)
    x = width/2;  //(8)
  y = height/2; //(9)
}

void draw() {
  if (cam.isStarted()){
   
   
   
    pushMatrix();  //(10)
    translate(x, y);  //(11)
    rotate(rectAngle); 
    image(cam, 0, 0);  //(3)
    
    popMatrix();  //(12)
    
    
  }
}

void onCameraPreviewEvent() {  //(4)
  cam.read();  //(5)
}

/*void mousePressed() {
  
  if (cam.isStarted())
  {
    cam.stop();  //(6)
  }
  else
    cam.start();
}
*/
void onRotate(float x, float y, float angle)  //(18)
{
  rectAngle += angle;
  println("ROTATE:" + angle);
}

void mouseDragged()  //(19)
{
//  if (abs(mouseX - x) < rectSize/2 && abs(mouseY - y) < rectSize/2)
 // {
 //   if (abs(mouseX - pmouseX) < rectSize/2) 
      x += mouseX - pmouseX;
//    if (abs(mouseY - pmouseY) < rectSize/2) 
      y += mouseY - pmouseY;
//  }
}
public boolean surfaceTouchEvent(MotionEvent event) {  //(20)
  //call to keep mouseX and mouseY constants updated
  super.surfaceTouchEvent(event);
  //forward events
  return gesture.surfaceTouchEvent(event);
}



