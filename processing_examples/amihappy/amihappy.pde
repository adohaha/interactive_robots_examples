import ketai.camera.*;
import ketai.cv.facedetector.*;
import android.graphics.Matrix;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.util.List;
import java.util.Arrays;
import java.util.stream.IntStream;
KetaiCamera cam;
Context context;
Activity act;
EmotionServiceRestClient emo_recog;
//import com.microsoft.projectoxford.*;

//
//import com.google.gson.Gson;
import com.microsoft.projectoxford.emotion.EmotionServiceClient;
import com.microsoft.projectoxford.emotion.EmotionServiceRestClient;
import com.microsoft.projectoxford.emotion.contract.FaceRectangle;
import com.microsoft.projectoxford.emotion.contract.RecognizeResult;
import com.microsoft.projectoxford.emotion.contract.Scores;
import com.microsoft.projectoxford.emotion.rest.EmotionServiceException;
//import com.microsoft.projectoxford.emotionsample.helper.ImageHelper;
//
//import com.microsoft.projectoxford.face.FaceServiceRestClient;
//import com.microsoft.projectoxford.face.contract.Face;
//import org.apache.commons.io.IOUtils;

import android.content.Context; 
import android.app.Activity;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
//import org.json.*;



List<RecognizeResult> result;

void setup() {
 fullScreen();
//size(640,480);
  orientation(PORTRAIT);
  cam = new KetaiCamera(this, 640, 480, 24);
   cam.setCameraID(1);  //  front camera
  rectMode(CENTER);  
  stroke(0, 255, 0);
  noFill();  
  act = this.getActivity();
  context = act.getApplicationContext();
 
  emo_recog=new EmotionServiceRestClient( "INSERT_KEY");
}

void draw() {
  background(0);
  
  if (cam != null)
  {
    translate(width/2, height/2);
    rotate(PI/2);
    image(cam, 0, 0, 640, 480);

    
  }
}

void image_recog(int [] cam_pixels) throws Exception {
}

void mousePressed ()
{
  if(!cam.isStarted()){
    cam.start();
  }
  else
  {
    int [] cam_pixels=cam.pixels;
    
   // IntStream is = Arrays.stream(cam_pixels);
    Bitmap bmp = Bitmap.createBitmap(cam.width, cam.height, Bitmap.Config.ARGB_8888);
    bmp.setPixels(cam_pixels, 0, cam.width, 0, 0, cam.width, cam.height);
    ByteArrayOutputStream output = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, output);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(output.toByteArray());
          Gson gson = new Gson();
        try{
         result=emo_recog.recognizeImage(inputStream);
         String json = gson.toJson(result);
         
         
     
         print("I am this happy:"+(result.get(0).scores.happiness));
         print(json);
        }catch(Exception e) {
        
         println(e);
        }
   
   
    
  
  }
}
 


void onCameraPreviewEvent()
{
  cam.read();
}
