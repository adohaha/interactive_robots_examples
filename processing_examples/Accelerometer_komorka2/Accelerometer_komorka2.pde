AccelerometerManager accel;


import android.view.MotionEvent;

import ketai.ui.*;

KetaiGesture gesture;
KetaiVibrate vibe;
float ax, ay, az;
int num = 100;
float mx[] = new float[num];
float my[] = new float[num];
int skuch=0;
class Pilka 
{
  PVector polozenie;
  PVector predkosc;
  PVector przyspieszenie;
  float dt;
  float staryc;
  float nowyc;
  float sredn;
  float skalax;
  float skalay;
float visco;



Pilka()
{
  polozenie= new PVector(0,0,0);
  predkosc= new PVector(0,0,0);
  przyspieszenie= new PVector(0,0,0);
  dt=0.0;

  staryc=millis();
  nowyc=millis();
  sredn=20;
  skalay=1280/(0.095);
  skalax=720/(0.06);
  visco=3.0;
  this.przeloz(width/2,height/2);
}
PVector gdzieWPix()
{
  PVector gdzie= new PVector(polozenie.x*skalax,polozenie.y*skalay,0);
  return gdzie;
}
void updatestate(float ax,float ay,float az)
{
  polozenie.add(PVector.mult(predkosc,dt));
  predkosc.add(PVector.mult(przyspieszenie,dt));
  przyspieszenie.set(-ax,ay,az);
  przyspieszenie.add(PVector.mult(predkosc,-1*visco));
  nowyc=millis();
  dt=(nowyc-staryc)/1000;
  staryc=nowyc;
  if( polozenie.x*skalax>width || polozenie.x*skalax<0 || polozenie.y*skalay > height || polozenie.y*skalay < 0)
  {
    vibe.vibrate();
  }
  else
  {
   vibe.stop();
  }
}
void pdraw()
{
  ellipse(polozenie.x*skalax,polozenie.y*skalay,sredn,sredn);
  //ellipse(polozenie.x*skalax,polozenie.y*skalay,sredn,sredn);
}
void przeloz(float ix,float iy)
{
  this.przeloz(ix,iy,0,0);
}
void przeloz(float ix,float iy,float px,float py)
{
  polozenie.set(ix/skalax,iy/skalay,0);
  predkosc.set(px,py,0);
}

void pilall(float ix,float iy,float iz)
{
  this.updatestate(ix,iy,iz);
  this.pdraw();
}


}
Pilka pil;
void setup() {
  accel = new AccelerometerManager(this);
  orientation(PORTRAIT);
    gesture = new KetaiGesture(this);
//  frameRate(24);
  pil= new Pilka();
  noLoop();
    smooth();
     vibe = new KetaiVibrate(this);
}


void draw() {
  background(0);
  
  fill(255);
  textSize(70);
  textAlign(CENTER, CENTER);
  text("x: " + nf(ax, 1, 2) + "\n" + 
       "y: " + nf(ay, 1, 2) + "\n" + 
       "z: " + nf(az, 1, 2)+ "\n" +
       "pred x: " + nf(pil.predkosc.x,1,2) + "\n" +
       "pred y: " + nf(pil.predkosc.y,1,2) + "\n"+
       "SKUCH: "+ skuch, 
       0, 0, width, height);
  mapa(0,0);
  pil.pilall(ax,ay,az);
  
  
   int which = frameCount % num;
  mx[which] = pil.polozenie.x*pil.skalax;
  my[which] = pil.polozenie.y*pil.skalay;
  
   for (int i = 0; i < num; i++) {
     int index = (which+1 + i) % num;
     fill(255, i,100,100-i);
    ellipse(mx[index], my[index], i/2, i/2);
  }
  
}


public void resume() {
  if (accel != null) {
    accel.resume();
  }
}

    
public void pause() {
  if (accel != null) {
    accel.pause();
  }
}


public void shakeEvent(float force) {
  println("shake : " + force);
}
void mouseDragged()
{
  pil.przeloz(mouseX,mouseY,-5*pmouseX/pil.skalax,-5*pmouseY/pil.skalay);
}

public void accelerationEvent(float x, float y, float z) {
//  println("acceleration: " + x + ", " + y + ", " + z);
  ax = x;
  ay = y;
  az = z;
  redraw();
}

void mouseReleased()
{
  skuch=skuch+1;
}

void mapa(int polx,int poly)
{
  int skala=50;
  int oldcol=g.fillColor;
float org= g.strokeWeight;
  float orgcol=g.strokeColor;
  fill(0);
  stroke(255);
  strokeWeight(1);
  rect (0,0,width*10/skala,10*height/skala);
  //rectMode(CENTER);
  PVector pozpil=pil.gdzieWPix();
  rect(5*width/skala,5*height/skala,width/skala,height/skala);
  strokeWeight(5);
  stroke(255,0,0);
  float xx=5*width/skala+pozpil.x/skala;
  float yy=5*height/skala+pozpil.y/skala;
  if (xx>0 && yy>0 && yy<height*10/skala && xx<width*10/skala)
  {
    
  point(5*width/skala+pozpil.x/skala,5*height/skala+pozpil.y/skala);
  }
  fill(oldcol);
  stroke(orgcol);
  strokeWeight(org);
 // g.strokeColor= orgcol;
 // g.strokeWeight = org;
}



public boolean surfaceTouchEvent(MotionEvent event) {

  //call to keep mouseX, mouseY, etc updated
  super.surfaceTouchEvent(event);

  //forward event to class for processing
  return gesture.surfaceTouchEvent(event);
}

