import ketai.ui.*;
import android.view.MotionEvent;

KetaiGesture gesture;
float rectSize = 100;
float rectAngle =0;
int x,y;
color c = color(255);
color bg = color(78,93,75);

void setup()
{
  orientation(LANDSCAPE);
  gesture = new KetaiGesture(this);
  textSize(32);
  textAlign(CENTER,BOTTOM);
  
  rectMode(CENTER);
  noStroke();
  x = width/2;
  y= height/2;
}

void draw()
{
  background(bg);
  pushMatrix();
  translate(x,y);
  rotate(rectAngle);
  fill(c);
  rect(0,0,rectSize,rectSize);
  popMatrix();
}

void onTap(float x, float y)
{
  text("SINGLE",x,y-10);
  println("SINGLE"+x+','+y);
}


void onDoubleTap(float x, float y)
{

if (rectSize > 100)
{rectSize = 3;
}
else{
rectSize = height - 100;
}
}

void onRotate(float x, float y, float angle)  //(18)
{
  rectAngle += angle;
  println("ROTATE:" + angle);
}

public boolean surfaceTouchEvent(MotionEvent event) {  //(20)
  //call to keep mouseX and mouseY constants updated
  super.surfaceTouchEvent(event);
  //forward events
  return gesture.surfaceTouchEvent(event);
}
