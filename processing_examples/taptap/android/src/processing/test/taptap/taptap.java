package processing.test.taptap;

import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import ketai.ui.*; 
import android.view.MotionEvent; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class taptap extends PApplet {




KetaiGesture gesture;
float rectSize = 100;
float rectAngle =0;
int x,y;
int c = color(255);
colog bg = color(78,93,75);

public void setup()
{
  orientation(LANDSCAPE);
  gesture = new KetaiGesture(this);
  textSize(32);
  textAlign(CENTER,BOTTOM);
  
  rectMode(CENTER);
  noStroke;
  x = width/2;
  y= height/2;
}

public void draw()
{
  background(bg);
  pushMatrix();
  translate(x,y);
  rotate(rectAngle);
  fill(c);
  rect(0,0,rectSize,rectSize);
  popMatrix();
}

public void onTap(float x, float y);
{
  text("SINGLE",x,y-10);
}

}
