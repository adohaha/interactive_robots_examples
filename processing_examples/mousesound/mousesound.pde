/**
  * This sketch demonstrates how to create synthesized sound with Minim 
  * using an AudioOutput and an Oscil. An Oscil is a UGen object, 
  * one of many different types included with Minim. For many more examples 
  * of UGens included with Minim, have a look in the Synthesis 
  * folder of the Minim examples.
  
  //changed to translate mouse position to sound
  */

import ddf.minim.*;
import ddf.minim.ugens.*;

Minim       minim;
AudioOutput out;
Oscil       wave;
Frequency frekk;
float mousian;
void setup()
{
  size(800, 600);
  
  minim = new Minim(this);
  
  // use the getLineOut method of the Minim object to get an AudioOutput object
  out = minim.getLineOut();
  
  // create a sine wave Oscil, set to 440 Hz, at 0.5 amplitude
  wave = new Oscil( 440, 0.5f, Waves.SINE );
  // patch the Oscil to the output
  wave.patch( out );
 // frekk = new Frequency.ofHertz(3);
}

void draw()
{
  background(0);
  stroke(255);
 // frekk.setAsHz(mouseX);
 mousian=mouseX;
 wave.setFrequency(mousian);
  wave.setAmplitude(mouseY/100.0);
  // draw the waveforms
  for(int i = 0; i < out.bufferSize() - 1; i++)
  {
    line( i, 50 + out.left.get(i)*50, i+1, 50 + out.left.get(i+1)*50 );
    line( i, 150 + out.right.get(i)*50, i+1, 150 + out.right.get(i+1)*50 );
  }
}
