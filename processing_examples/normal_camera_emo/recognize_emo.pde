import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
//import org.json.*;


import com.microsoft.projectoxford.emotion.EmotionServiceClient;
import com.microsoft.projectoxford.emotion.EmotionServiceRestClient;
import com.microsoft.projectoxford.emotion.contract.FaceRectangle;
import com.microsoft.projectoxford.emotion.contract.RecognizeResult;
import com.microsoft.projectoxford.emotion.contract.Scores;
import com.microsoft.projectoxford.emotion.rest.EmotionServiceException;


List<RecognizeResult> result;
EmotionServiceRestClient emo_recog;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;




void recognizeFromCamera(PImage cam)
{
  
   //int [] cam_pixels=cam.pixels;
   BufferedImage native_image= (BufferedImage) cam.getNative();
    ByteArrayOutputStream output = new ByteArrayOutputStream();
    
        try{
        ImageIO.write(native_image, "jpg", output);
        
        ByteArrayInputStream inputStream = new ByteArrayInputStream(output.toByteArray());
          Gson gson = new Gson();
        try{
         result=emo_recog.recognizeImage(inputStream);
         String json = gson.toJson(result);
         
         
    
         print("I am this happy:"+ result.get(0).scores.happiness);
         print(json);
        }catch(Exception e) {
        
         println(e);
        }
   
        }catch(Exception e)
        {
          println(e);
        }
    
    
}
