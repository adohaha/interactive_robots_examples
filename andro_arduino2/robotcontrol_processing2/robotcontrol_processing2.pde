/* A simple example to control your arduino board via bluetooth of your android smartphone or tablet. Tested with Android 4.
Requirements:
Arduino Board
Bluetooth shield
Android Smartphone (Android 2.3.3 min.)
Ketai library for processing
Jumper D0 to TX
Jumper D1 to RX
Set bluetooth, bluetooth admin and internet sketch permissions in processing.
Processing Code:
*/
 
//required for BT enabling on startup
 
import android.content.Intent;
import android.os.Bundle;

import ketai.ui.*;
import ketai.net.*;
import ketai.sensors.*;
PFont fontMy;

short xx=0;
short yy=0;

boolean isStarted=false;
 int ax,ay;
 
 PVector accelerometer_values;
 KetaiSensor sensor;
 
void setup() {
  fullScreen();
 
 frameRate(10);
 orientation(PORTRAIT);
 background(0);
  
 //start listening for BT connections
 bt.start();
 //at app start select device…
 isConfiguring = true;
 //font size
 fontMy = createFont("SansSerif", 40);
 textFont(fontMy);
 
ax=1;
ay=1;

// starting to read from sensors
 sensor = new KetaiSensor(this);
  sensor.start();
accelerometer_values = new PVector();
}
 

void draw() {
 //at app start select device
 if (isConfiguring) // first we need to setup bluetooth connection
 {
  ArrayList names;
  background(78, 93, 75);
  klist = new KetaiList(this, bt.getPairedDeviceNames());
  isConfiguring = false;
 }
 else
 {



 if(isStarted)
 {
  formandsend('p',600*accelerometer_values.x,600* accelerometer_values.y); // sending speeds from accelerometer values
    background(2,30,2); //is opened
    PVector mouseaccel  = new PVector(60*accelerometer_values.x,60*accelerometer_values.y);
  PVector center = new PVector(width/2,height/2);
//PVector subtraction!
  mouseaccel.sub(center);
//Draw a line to represent the vector.
  //translate(width/2,height/2);
 // line(0,0,mouseaccel.x,mouseaccel.y);
   stroke(255, 0, 0);
  arrowLine(0, 0, mouseaccel.x, mouseaccel.y,0,  atan2(mouseaccel.x,mouseaccel.y), false);
   
 } 
 else
 {
   background(100,20,0); // is closed
 }
 
  fill(255);
  noStroke();
  textAlign(LEFT);
  text(info, 20, 104);
 }
}
 




// different events


void onAccelerometerEvent(float x, float y, float z, long time, int accuracy)
{
  accelerometer_values.set(x, y, z);
}

void mouseReleased() {
  if(isStarted)
  {
    // we have to stop!
    formandsend('p',0,0);
  formandsend('s',0,0);
  isStarted=false;
  info="stop mode \n";
  }
  else
  {
      formandsend('t',0,0);
  isStarted=true;
  }
  
} 