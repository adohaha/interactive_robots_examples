import android.os.Environment;

Table csv;
ArrayList<PVector> points = new ArrayList<PVector>();

void setup()
{
  orientation(LANDSCAPE);
  noStroke();
  textSize(24);
  textAlign(CENTER);
  
  try {
    csv = new Table(new File(
    Environment.getExternalStorageDirectory().getAbsolutePath() +
    "/data.csv"));
  } 
  catch (Exception e) {
    csv = new Table();
  }
  for (int row = 1; row < csv.getRowCount(); row++)
  {
    points.add(new PVector(csv.getInt(row, 0), csv.getInt(row, 1), 0));
  }
}

void draw()
{
  background(78, 93, 75);
  for (int i = 0; i < points.size(); i++)
  {
    PVector p = points.get(i);
    ellipse(p.x, p.y, 5, 5);  
  } 
  text("Number of points: " + points.size(), width/2, 50);
}

void keyPressed()
{
  csv.writeCSV(new File(
  Environment.getExternalStorageDirectory().getAbsolutePath() +
  "/data.csv"));
}

void mouseDragged()
{
  points.add(new PVector(mouseX, mouseY, 0));
  String[] data = { 
    Integer.toString(mouseX), Integer.toString(mouseY)
  };
  csv.addRow();
  csv.setRow(csv.getRowCount()-1, data);
}
