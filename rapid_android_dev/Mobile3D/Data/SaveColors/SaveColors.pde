String[] txt = new String[1];
int value;

void setup() {
  textAlign(CENTER, CENTER);
  textSize(36);
  loadValue();
}

void draw() {
  background(78, 93, 75);
  text(value, width/2, height/2);
}

void mousePressed() {
  value++;
  saveValue(value);
}

void saveValue(int val) {
  txt[0] = val+"";
  saveStrings("\\sdcard\\memory.txt", txt);
}
void loadValue() {
  try {
    txt = loadStrings("\\sdcard\\memory.txt");
    value = int(txt[0]);
  }
  catch (NullPointerException e) {
    println("No memory.txt file found...");
  }
}

