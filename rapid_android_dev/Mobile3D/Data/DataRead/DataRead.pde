Table colors;

void setup()
{
  textSize(24);
  rectMode(CENTER);
  textAlign(CENTER, CENTER);
  noStroke();
  noLoop();
  colors = new Table(this, "colors.csv");
  int count = colors.getRowCount();
  for (int row = 0; row < count; row++)
  {
    fill(unhex("FF"+colors.getString(row, 1)));
    rect(width/2, height/count/2, width, height/count);
    fill(255);
    text(colors.getString(row, 0), width/2, height/count/2);
    translate(0, height/count);
  }
}

