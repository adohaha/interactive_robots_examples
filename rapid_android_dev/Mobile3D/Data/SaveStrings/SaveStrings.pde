import java.io.File;
import android.os.Environment;
 
String[] txt = new String[1];
String path;
int value;

void setup() {
  textAlign(CENTER, CENTER);
  textSize(36);
  path = Environment.getExternalStorageDirectory().getAbsolutePath() 
    + File.separator;
  println(path);
  loadValue();
}

void draw() {
  background(78, 93, 75);
  text(value, width/2, height/2);
}

void mousePressed() {
  value++;
  saveValue(value);
}

void saveValue(int val) {
  txt[0] = val+"";
  saveStrings(path +"memory.txt", txt);
}
void loadValue() {
  try {
    txt = loadStrings(path +"memory.txt");
    value = int(txt[0]);
  }
  catch (NullPointerException e) {
    // problem if file doesn't exist
    println("No memory.txt file found...");
    txt = new String[1];
    txt[0] = "0";
  }
}

