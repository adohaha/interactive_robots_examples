//program that shows x,y mouse values and sends it to arduino.
// arduino robot in turn, sends encoder values
import processing.serial.*;

Serial commPort;

// 
int leftwheelticks=0;
int rightwheelticks=0;


// set some font
PFont fontMy;
String info;
void setup()
{
  size(255,255);
 // String portName="/dev/ttyS99"; //simbolic link trick
  String portName="/dev/ttyACM0";
  commPort= new Serial(this,portName,57600); // setting communication with baudrate of 57600
 commPort.bufferUntil('\n'); //serial event generated only when new line

 fontMy = createFont("SansSerif", 10);
 textFont(fontMy); 
}

void draw(){ //this will loop, enabling new picture creation
background(0); //clean 
drawWheels();
commPort.write('X');
commPort.write(mouseX);
commPort.write(mouseY);
info="mouseX:"+mouseX+ "mouseY:"+mouseY;
textAlign(LEFT);
  text(info, 20, 104);

// rotated rectangle  
//rotate(0.2);  
//rect(50, 50, 100, 100);  
}

void serialEvent (Serial myPort) {

String inString = myPort.readStringUntil('\n');

if (inString != null) {
        println(inString);
       
        String[] parts = inString.split("\\s+"); //split by space
        
        try{
        rightwheelticks=int(parts[0]);
        leftwheelticks=int(parts[1]);
        println(leftwheelticks);
        }
        catch (Exception e){
        
        }
//photoVal = int(inString);
        }

}
void drawWheels()
{
  fill(255,0,0);
int leftsign=leftwheelticks>0 ? 1 : -1; // gets sighn of ticks
int rightsign=rightwheelticks>0 ? 1 : -1;

drawRotatedRect(leftsign*(abs(leftwheelticks)%48)/(2*PI),50,50,25,25); 
drawRotatedRect(rightsign*(abs(rightwheelticks)%48)/(2*PI),200,50,25,25);
fill(0,0,0);
  
  
}
void drawRotatedRect(float radd,int x, int y, int widthh, int heightt)
{
  
  translate(x,y);
rotate(radd);  
rect(-widthh/2, -heightt/2, widthh, heightt);  
rotate(-radd);
translate(-x,-y);
}
  

