//program that shows x,y mouse values and sends it to arduino.
// arduino robot in turn, sends encoder values
import android.content.Intent;
import android.os.Bundle;

import ketai.ui.*;
import ketai.net.*;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder; 

// 
int leftwheelticks=0;
int rightwheelticks=0;


// set some font
PFont fontMy;
String info;
void setup()
{
  size(255,255);
 // String portName="/dev/ttyS99"; //simbolic link trick
//  String portName="/dev/ttyACM0";
 // commPort= new Serial(this,portName,57600); // setting communication with baudrate of 57600
// commPort.bufferUntil('\n'); //serial event generated only when new line
bt.start();
isConfiguring = true;
 fontMy = createFont("SansSerif", 10);
 textFont(fontMy); 
}
byte message[]= new byte[3];
void draw(){
 if (isConfiguring)
 {
  ArrayList names;
  background(78, 93, 75);
  klist = new KetaiList(this, bt.getPairedDeviceNames());
  isConfiguring = false;
 }
 else
 {
 
  //this will loop, enabling new picture creation
background(0); //clean 
drawWheels();
//form message:
message[0]='X';
message[1]=(byte)mouseX;
message[2]=(byte)mouseY;
bt.broadcast(message);

info="mouseX:"+mouseX+ "mouseY:"+mouseY;
textAlign(LEFT);
  text(info, 20, 104);

// rotated rectangle  
//rotate(0.2);  
//rect(50, 50, 100, 100);  
 }
}


void drawWheels()
{
  fill(255,0,0);
int leftsign=leftwheelticks>0 ? 1 : -1; // gets sighn of ticks
int rightsign=rightwheelticks>0 ? 1 : -1;

drawRotatedRect(leftsign*(abs(leftwheelticks)%48)/(2*PI),50,50,25,25); 
drawRotatedRect(rightsign*(abs(rightwheelticks)%48)/(2*PI),200,50,25,25);
fill(0,0,0);
  
  
}
void drawRotatedRect(float radd,int x, int y, int widthh, int heightt)
{
  
  translate(x,y);
rotate(radd);  
rect(-widthh/2, -heightt/2, widthh, heightt);  
rotate(-radd);
translate(-x,-y);
}
  

