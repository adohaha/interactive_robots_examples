#include <Sensor.h>
#include <Motor.h>
#include <Encoders.h>
#include <Spereg.h>






Motor wheelL(6,7); //speed pin, direction pin
Motor wheelR(5,4);
Encoders enkodery;

void setup(void) 
{ 
  Serial.begin(57600);      //Set Baud Rate
 // Serial.println("Run keyboard control");
  wheelR.init();
  wheelL.init();
enkodery.init(8,9,2,12); //rightA, rightB, leftA, leftB
} 
unsigned long lastcontroltime=0; 
void loop(void) 
{
  char controlchar;
  if(Serial.available()>=3){ 
    lastcontroltime=millis()+500;
    controlchar=Serial.read();
    if(controlchar=='X')
    {
      wheelR.setPWM(Serial.read());
      wheelL.setPWM(Serial.read());
    }
  }
  if(millis()>lastcontroltime)
  {
    wheelR.setPWM(0);
      wheelL.setPWM(0);
  }
  
  // sending encoder values as ASCII, right impulses, space, left impulses, new line sign
  Serial.print(enkodery.impulsyPrawy());
  Serial.print(" ");
  Serial.println(enkodery.impulsyLewy());

	//static int impulsyLewy();
}
