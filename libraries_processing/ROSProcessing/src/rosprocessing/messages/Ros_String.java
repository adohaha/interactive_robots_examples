

/**
 * A generic specified Java Bean for capturing many of the primitive data-type messages used by ROS in the std_msgs
 * package. The class has a single public data member called "data" that belongs to the specified primitive type.
 * @author James MacGlashan.
 */
package rosprocessing;

public class Ros_String
{
  public String data;
  
  
    public Ros_String() {
    this.data="";
   
  }
 public Ros_String(String data) {
    this.data=data;
  }
  
  public void print(String name) {
    System.out.println(name+this.data);
  }
}
